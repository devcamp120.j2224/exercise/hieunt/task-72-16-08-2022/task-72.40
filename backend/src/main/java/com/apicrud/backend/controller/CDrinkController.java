package com.apicrud.backend.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.apicrud.backend.model.CDrink;
import com.apicrud.backend.repository.IDrinkRepository;
import com.apicrud.backend.service.CDrinkService;

@RestController
@RequestMapping("/drinks")
@CrossOrigin(value = "*", maxAge = -1)
public class CDrinkController {
    @Autowired
    IDrinkRepository pDrinkRepository;
    @Autowired
    CDrinkService CDrinkService;
    // Lấy danh sách drink CÓ dùng service.
    @GetMapping("")
    public ResponseEntity<List<CDrink>> getAllDrinksByService() {
        try {
            return new ResponseEntity<>(CDrinkService.getDrinkList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }
    // Lấy drink theo {id} CÓ dùng service
    @GetMapping("/{id}")
	public ResponseEntity<Object> getDrinkByIdWithService(@PathVariable("id") long id) {
		CDrink existedDrink = CDrinkService.getDrinkById(id);
		if (existedDrink != null) {
			return new ResponseEntity<>(existedDrink, HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Drink not found", HttpStatus.NOT_FOUND);
		}
	}
    // Tạo MỚI drink CÓ dùng service sử dụng phương thức POST
    @PostMapping("") 
	public ResponseEntity<Object> createDrinkWithService(@Valid @RequestBody CDrink pDrink) {
		try {
			CDrink newDrink = CDrinkService.createDrink(pDrink);
			return new ResponseEntity<>(pDrinkRepository.save(newDrink), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Drink: "+e.getCause().getCause().getMessage());
		}
	}
    // Sửa/update drink theo {id} CÓ dùng service, sử dụng phương thức PUT
    @PutMapping("/{id}") 
	public ResponseEntity<Object> updateDrinkByIdWithService(@PathVariable("id") long id, @Valid @RequestBody CDrink pDrink) {
		CDrink updatedDrink = CDrinkService.updateDrinkById(id, pDrink);
		if (updatedDrink != null) {
			try {
				return new ResponseEntity<>(pDrinkRepository.save(updatedDrink), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Drink:"+e.getCause().getCause().getMessage());
			}
		} else {
			return new ResponseEntity<>("Drink not found", HttpStatus.NOT_FOUND);
		}
	}
    // Xoá/delete drink theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteDrinkById(@PathVariable("id") long id) {
		try {
			pDrinkRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
