package com.apicrud.backend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.apicrud.backend.model.*;
import com.apicrud.backend.repository.*;


@RequestMapping("/orders")
@RestController
@CrossOrigin(value = "*" , maxAge = -1)

public class OrderController { 
    @Autowired
    IUserRepository iUserRepository;
    @Autowired
    IOrderRepository iOrderRepository;

    @GetMapping("")
    public List<COrder> getAllOrders() {
        return iOrderRepository.findAll();
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<Object> getOrdersByUserId(@PathVariable long userId) {
        Optional<CUser> existedUser = iUserRepository.findById(userId);
        if(existedUser.isPresent()) {
            return new ResponseEntity<>(existedUser.get().getOrders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable long id) {
        Optional<COrder> foundOrder = iOrderRepository.findById(id);
        if(foundOrder.isPresent()) {
            return new ResponseEntity<>(foundOrder.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Order not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/user/{userId}")
    public ResponseEntity<Object> createOrder(@PathVariable long userId, @RequestBody COrder paramOrder) {
        Optional<CUser> existedUser = iUserRepository.findById(userId);
        if(existedUser.isPresent()) {
            try {
                COrder newOrder = new COrder();
                newOrder.setOrderCode(paramOrder.getOrderCode());
                newOrder.setVoucherCode(paramOrder.getVoucherCode());
                newOrder.setPizzaSize(paramOrder.getPizzaSize());
                newOrder.setPizzaType(paramOrder.getPizzaType());
                newOrder.setPrice(paramOrder.getPrice());
                newOrder.setPaid(paramOrder.getPaid());
                newOrder.setUser(existedUser.get());
                return new ResponseEntity<>(iOrderRepository.save(newOrder), HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create Order: " + e.getCause().getCause().getMessage());
            }
        } else
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Object> updateOrderById(@PathVariable long id, @RequestBody COrder paramOrder) {
        Optional<COrder> existedOrderData = iOrderRepository.findById(id);
        if(existedOrderData.isPresent()) {
            try {
                COrder existedOrder = existedOrderData.get();
                existedOrder.setVoucherCode(paramOrder.getVoucherCode());
                existedOrder.setPizzaSize(paramOrder.getPizzaSize());
                existedOrder.setPizzaType(paramOrder.getPizzaType());
                existedOrder.setPrice(paramOrder.getPrice());
                existedOrder.setPaid(paramOrder.getPaid());
                return new ResponseEntity<>(iOrderRepository.save(existedOrder), HttpStatus.OK);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update Order: " + e.getCause().getCause().getMessage());
            }
        } else
            return new ResponseEntity<>("Order not found", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable long id) {
        Optional<COrder> existedOrder = iOrderRepository.findById(id);
        if(existedOrder.isPresent()) {
            try {
                iOrderRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                    .body("Failed to Delete Order: " + e.getCause().getCause().getMessage());
            }
        } else
            return new ResponseEntity<>("Order not found", HttpStatus.NOT_FOUND);
    }

}
