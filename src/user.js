$(document).ready(function() {
    'use strict';
    // VÙNG 1: Biến toàn cục 
    var gDatabase = []; // biến lưu all dữ liệu
    var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
    var gBaseUrl = "http://localhost:8080/users";
    var gTable = $("#user-table").DataTable({
      "columns": [
        { "data" : "id" },
        { "data" : "fullName" },
        { "data" : "email" },
        { "data" : "phone" },
        { "data" : "address" },
        { "data": 'action' },
      ],
      columnDefs: [
        {
          targets: 5,
          defaultContent: `
            <div class="d-inline-flex">
              <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
              <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
            </div>
          `
        }
      ]
    })
  
    // VÙNG 2: gán sự kiện cho các phần tử
    $(document).ready(function() { // hàm chạy khi load trang
      callAjaxApiGetAllUsers();
    })
    $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
    gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
      onBtnUpdateDataClick(this)
    })
    gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
      onBtnDeleteDataClick(this)
    })
    $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
    $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
    $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
    
    // VÙNG 3: hàm xử lý sự kiện
    function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
      $("#modal-add-data").modal("show");
    }
    function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
      "use strict";
      $("#modal-update-data").modal("show");
      var vCurrentRow = $(paramButtonElement).closest("tr");
      var vRowData = gTable.row(vCurrentRow).data();
      gSelectedId = vRowData.id;
      console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
      loadDataToModalUpdateData(vRowData);
    }
    function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
      "use strict";
      $("#modal-delete-data").modal("show");
      var vCurrentRow = $(paramButtonElement).closest("tr");
      var vRowData = gTable.row(vCurrentRow).data();
      gSelectedId = vRowData.id;
      $("#span-data-id").html(gSelectedId);
      console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
    }
    function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
      gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
      // B1: đọc dữ liệu từ form
      var vUserData = readDataFromModalAddData();
      // B2: validate dữ liệu
      var vIsValid = validateData(vUserData);
      if (vIsValid == true) {
        // B3: gọi api
        callAjaxApiAddNewUser(vUserData);
        resetModalAddData();
        $("#modal-add-data").modal("hide");
      }
    }
    function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
      // B1: đọc dữ liệu từ modal update data
      var vUserData = readDataFromModalUpdateData();
      // B2: validate dữ liệu
      var vIsValid = validateData(vUserData);
      if(vIsValid == true) {
        // B3: gọi api
        callAjaxApiUpdateUserById(gSelectedId, vUserData);
        $("#modal-update-data").modal("hide")
      }
    }
    function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
      callAjaxApiDeleteUserById(gSelectedId);
      $("#modal-delete-data").modal("hide")
    }
  
    // VÙNG 4: hàm dùng chung
    function callAjaxApiGetAllUsers() { // gọi api lấy list all dữ liệu
      $.ajax({
        url: gBaseUrl,
        type: "GET",
        async: false,
        dataType: "json",
        success: function(res) {
          console.log(res);
          gDatabase = res;
          loadListUserToTable(gDatabase);
        },
        error: function(err) {
          console.log(err.response);
        }
      })
    }
    function loadListUserToTable(paramListUser) { // đổ list dữ liệu vào table
      "use strict";
      gTable.clear();
      gTable.rows.add(paramListUser);
      gTable.draw();
    }
    function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
      var vUserData = {
        fullName: $("#input-fullName-modal-add-data").val().trim(),
        email: $("#input-email-modal-add-data").val().trim(),
        phone: $("#input-phone-modal-add-data").val().trim(),
        address: $("#input-address-modal-add-data").val().trim(),
      };			
      console.log("Thông tin User đọc được là:");
      console.log(vUserData);
      return vUserData
    }
    function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
      var vUserData = {
        fullName: $("#input-fullName-modal-update-data").val().trim(),
        email: $("#input-email-modal-update-data").val().trim(),
        phone: $("#input-phone-modal-update-data").val().trim(),
        address: $("#input-address-modal-update-data").val().trim(),
      };			
      console.log("Thông tin User đọc được là:");
      console.log(vUserData);
      return vUserData
    }
    function loadDataToModalUpdateData(paramUser) { // đổ dữ liệu vào modal update data
      "use strict";
      $("#input-fullName-modal-update-data").val(paramUser.fullName);
      $("#input-email-modal-update-data").val(paramUser.email);
      $("#input-phone-modal-update-data").val(paramUser.phone);
      $("#input-address-modal-update-data").val(paramUser.address);
    }
    function validateData(paramUserData) { // kiểm tra dữ liệu từ 2 modal
      "use strict";
      var vIsValid = false;
      if (paramUserData.fullName == "") {
        alert("Chưa nhập Họ tên")
      }
      else if (paramUserData.email == "") {
        alert("Chưa nhập Email")
      }
      else if (validateEmail(paramUserData.email) == false) {
        alert("Email không đúng định dạng")
      }
      else if (checkEmailDaTonTai(paramUserData.email, gSelectedId)) {
        alert("Đã tồn tại Email này");
      }
      else if (paramUserData.phone == "") {
        alert("Chưa nhập Số ĐT")
      }
      else if (isNaN(paramUserData.phone) || paramUserData.phone < 0 || Number.isInteger(Number(paramUserData.phone)) == false) {
        alert("Số ĐT phải là dãy số nguyên dương")
      }
      else if (paramUserData.address == "") {
        alert("Chưa nhập Địa chỉ")
      }
      else {
        vIsValid = true;
      }
      return vIsValid
    }
    function validateEmail(paramEmail) { 
      var vRegEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      return vRegEx.test(paramEmail);
    }
    function checkEmailDaTonTai(paramEmail, paramId) {
      "use strict";
      // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
      var vIsExisted = false;
      var bI = 0;
      while (vIsExisted == false && bI < gDatabase.length) {
        if (gDatabase[bI].email == paramEmail && gDatabase[bI].id != paramId) {
          // nếu trùng email của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu của chính item đó, nên ko xem là bị trùng email
          // phải trùng email của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng email với item khác,
          // hoặc đang add dữ liệu (vì khi add dữ liệu, paramId luôn là 0 nên luôn khác id của item)
          vIsExisted = true
        }
        else {
          bI ++
        }
      }
      return vIsExisted
    }
    function resetModalAddData() {
      $("#input-fullName-modal-add-data").val("");
      $("#input-email-modal-add-data").val("");
      $("#input-phone-modal-add-data").val("");
      $("#input-address-modal-add-data").val("");
    }
    function callAjaxApiAddNewUser(paramUserData) { // gọi api thêm dữ liệu
      "use strict";
      $.ajax({
        async: false,
        url: gBaseUrl,
        type: "POST",
        contentType: "application/json; charset=UTF-8",
        data: JSON.stringify(paramUserData),
        success: function(res) {
          callAjaxApiGetAllUsers();
          console.log("Tạo User mới thành công. Response là:");
          console.log(res);
          alert("Tạo User mới thành công! Id mới là: " + res.id);
        },
        error: function(err){
          console.log(err.response);
          alert("Tạo User mới không thành công! Xem console");
        }
      })
    }  
    function callAjaxApiUpdateUserById(paramId, paramUserData) { // gọi api cập nhật dữ liệu theo id
      "use strict";
      $.ajax({
        async: false,
        url: gBaseUrl + "/" + paramId,
        type: "PUT",
        contentType: "application/json; charset=UTF-8",
        data: JSON.stringify(paramUserData),
        success: function(res) {
          callAjaxApiGetAllUsers();
          console.log("Update thành công cho User có id " + paramId + ". Response là:");
          console.log(res);
          alert("Update thành công cho User có id " + paramId);
        },
        error: function(err){
          console.log(err.response);
          alert("Update không thành công. Xem console");
        }
      })
    }
    function callAjaxApiDeleteUserById(paramUserId) { // gọi api xóa dữ liệu theo id
      "use strict";
      $.ajax({
        async: false,
        url: gBaseUrl + "/" + paramUserId,
        type: "DELETE",
        dataType: "json",
        success: function(res) {
          callAjaxApiGetAllUsers();
          console.log("Xóa User có id " + paramUserId + " thành công. Response là:");
          console.log(res);
          alert("Xóa User có id " + paramUserId + " thành công!");
        },
        error: function(err){
          console.log(err.response);
          alert("Xóa User có id " + paramUserId + " không thành công!");
        }
      })
    }
  })
  