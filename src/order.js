$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gBaseUrl = "http://localhost:8080/orders";
  var gTable = $("#order-table").DataTable({
    "columns": [
      { "data" : "id" },
      { "data" : "orderCode" },
      { "data" : "pizzaSize" },
      { "data" : "pizzaType" },
      { "data" : "voucherCode" },
      { "data" : "price" },
      { "data" : "paid" },
      { "data" : "userId" },
      { "data": 'action' },
    ],
    columnDefs: [
      {
        targets: 8,
        defaultContent: `
          <div class="d-inline-flex">
            <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
            <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
          </div>
        `
      }
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxApiGetAllOrders();
    callAjaxApiGetAllUsers();
  })
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
    onBtnUpdateDataClick(this)
  })
  gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
    onBtnDeleteDataClick(this)
  })
  $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
  $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
  $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
  
  // VÙNG 3: hàm xử lý sự kiện
  function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
    $("#modal-add-data").modal("show");
  }
  function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    "use strict";
    $("#modal-update-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
    loadDataToModalUpdateData(vRowData);
  }
  function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
    "use strict";
    $("#modal-delete-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    $("#span-data-id").html(gSelectedId);
    console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
  }
  function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vOrderData = readDataFromModalAddData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vOrderData);
    if (vIsValid == true) {
      // B3: gọi api
      callAjaxApiAddNewOrder(vOrderData);
      resetModalAddData();
      $("#modal-add-data").modal("hide");
    }
  }
  function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
    // B1: đọc dữ liệu từ modal update data
    var vOrderData = readDataFromModalUpdateData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vOrderData);
    if(vIsValid == true) {
      // B3: gọi api
      callAjaxApiUpdateOrderById(gSelectedId, vOrderData);
      $("#modal-update-data").modal("hide")
    }
  }
  function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
    callAjaxApiDeleteOrderById(gSelectedId);
    $("#modal-delete-data").modal("hide")
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxApiGetAllOrders() { // gọi api lấy list all dữ liệu Order
    $.ajax({
      url: gBaseUrl,
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadListOrderToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListOrderToTable(paramListOrder) { // đổ list dữ liệu Order vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramListOrder);
    gTable.draw();
  }
  function callAjaxApiGetAllUsers() { // gọi api lấy list all dữ liệu User
    $.ajax({
      url: "http://localhost:8080/users",
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        loadListUserToSelect(res);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListUserToSelect(paramListUser) { // đổ list dữ liệu User vào các select
    "use strict";
    for (let bUser of paramListUser) {
      $(".select-user").each(function() {
        $(this).append($("<option>").val(bUser.id).html(bUser.id + " " + bUser.fullName));
      })
    };
  }
  function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
    var vOrderData = {
      userId: $("#select-user-modal-add-data").val().trim(),
      orderCode: $("#input-orderCode-modal-add-data").val().trim(),
      pizzaSize: $("#input-pizzaSize-modal-add-data").val().trim(),
      pizzaType: $("#input-pizzaType-modal-add-data").val().trim(),
      voucherCode: $("#input-voucherCode-modal-add-data").val().trim(),
      price: $("#input-price-modal-add-data").val().trim(),
      paid: $("#input-paid-modal-add-data").val().trim(),
    };			
    console.log("Thông tin Order đọc được là:");
    console.log(vOrderData);
    return vOrderData
  }
  function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
    var vOrderData = {
      pizzaSize: $("#input-pizzaSize-modal-update-data").val().trim(),
      pizzaType: $("#input-pizzaType-modal-update-data").val().trim(),
      voucherCode: $("#input-voucherCode-modal-update-data").val().trim(),
      price: $("#input-price-modal-update-data").val().trim(),
      paid: $("#input-paid-modal-update-data").val().trim(),
    };			
    console.log("Thông tin Order đọc được là:");
    console.log(vOrderData);
    return vOrderData
  }
  function loadDataToModalUpdateData(paramOrder) { // đổ dữ liệu vào modal update data
    "use strict";
    $("#select-user-modal-update-data").val(paramOrder.userId),
    $("#input-orderCode-modal-update-data").val(paramOrder.orderCode);
    $("#input-pizzaSize-modal-update-data").val(paramOrder.pizzaSize);
    $("#input-pizzaType-modal-update-data").val(paramOrder.pizzaType);
    $("#input-voucherCode-modal-update-data").val(paramOrder.voucherCode);
    $("#input-price-modal-update-data").val(paramOrder.price);
    $("#input-paid-modal-update-data").val(paramOrder.paid);
  }
  function validateData(paramOrderData) { // kiểm tra dữ liệu từ 2 modal
    "use strict";
    var vIsValid = false;
    if (paramOrderData.userId == "") {
      alert("Chưa chọn User")
    }
    else if (paramOrderData.orderCode == "") {
      alert("Chưa nhập Mã Order")
    }
    else if (checkOrderCodeDaTonTai(paramOrderData.orderCode, gSelectedId)) {
      alert("Đã tồn tại Mã Order này");
    }
    else if (paramOrderData.pizzaSize == "") {
      alert("Chưa nhập Cỡ Pizza")
    }
    else if (paramOrderData.pizzaType == "") {
      alert("Chưa nhập Loại Pizza")
    }
    else if (paramOrderData.price == "") {
      alert("Chưa nhập Giá")
    }
    else if (paramOrderData.price < 0 || !Number.isInteger(Number(paramOrderData.price))) {
      alert("Giá phải là số nguyên dương")
    }
    else if (paramOrderData.paid == "") {
      alert("Chưa nhập Tiền Đã Thanh toán")
    }
    else if (paramOrderData.paid < 0 || !Number.isInteger(Number(paramOrderData.paid))) {
      alert("Tiền Đã Thanh toán phải là số nguyên dương")
    }
    else {
      vIsValid = true;
    }
    return vIsValid
  }
  function checkOrderCodeDaTonTai(paramOrderCode, paramId) {
    "use strict";
    // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
    var vIsExisted = false;
    var bI = 0;
    while (vIsExisted == false && bI < gDatabase.length) {
      if (gDatabase[bI].orderCode == paramOrderCode && gDatabase[bI].id != paramId) {
        // nếu trùng mã của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu của chính item đó, nên ko xem là bị trùng mã
        // phải trùng mã của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng mã với item khác,
        // hoặc đang add dữ liệu (vì khi add dữ liệu, paramId luôn là 0 nên luôn khác id của item)
        vIsExisted = true
      }
      else {
        bI ++
      }
    }
    return vIsExisted
  }
  function resetModalAddData() {
    $("#select-user-modal-add-data").val(""),
    $("#input-orderCode-modal-add-data").val("");
    $("#input-pizzaSize-modal-add-data").val("");
    $("#input-pizzaType-modal-add-data").val("");
    $("#input-voucherCode-modal-add-data").val("");
    $("#input-price-modal-add-data").val("");
    $("#input-paid-modal-add-data").val("");
  }
  function callAjaxApiAddNewOrder(paramOrderData) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/user/" + paramOrderData.userId,
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramOrderData),
      success: function(res) {
        callAjaxApiGetAllOrders();
        console.log("Tạo Order mới thành công. Response là:");
        console.log(res);
        alert("Tạo Order mới thành công! Id mới là: " + res.id);
      },
      error: function(err){
        console.log(err.response);
        alert("Tạo Order mới không thành công! Xem console");
      }
    })
  }  
  function callAjaxApiUpdateOrderById(paramId, paramOrderData) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramOrderData),
      success: function(res) {
        callAjaxApiGetAllOrders();
        console.log("Update thành công cho Order có id " + paramId + ". Response là:");
        console.log(res);
        alert("Update thành công cho Order có id " + paramId);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callAjaxApiDeleteOrderById(paramOrderId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramOrderId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        callAjaxApiGetAllOrders();
        console.log("Xóa Order có id " + paramOrderId + " thành công. Response là:");
        console.log(res);
        alert("Xóa Order có id " + paramOrderId + " thành công!");
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa Order có id " + paramOrderId + " không thành công!");
      }
    })
  }
})
