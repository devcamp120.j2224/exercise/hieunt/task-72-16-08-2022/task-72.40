$(document).ready(function() {
  'use strict';
  // VÙNG 1: Biến toàn cục 
  var gDatabase = []; // biến lưu all dữ liệu
  var gSelectedId = 0; // biến lưu id dữ liệu đã chọn
  var gBaseUrl = "http://localhost:8080/menus";
  var gTable = $("#menu-table").DataTable({
    "columns": [
      { "data" : "id" },
      { "data" : "kichCo" },
      { "data" : "duongKinh" },
      { "data" : "suon" },
      { "data" : "salad" },
      { "data" : "soLuongNuoc" },
      { "data" : "thanhTien" },
      { "data": 'action' },
    ],
    columnDefs: [
      {
        targets: 7,
        defaultContent: `
          <div class="d-inline-flex">
            <button class="btn btn-link btn-update-data" title='Chỉnh sửa'><i class='far fa-edit'></i></button>
            <button class="btn btn-link btn-delete-data" title="Xóa"><i class="fas fa-trash-alt text-danger"></i></button>
          </div>
        `
      }
    ]
  })

  // VÙNG 2: gán sự kiện cho các phần tử
  $(document).ready(function() { // hàm chạy khi load trang
    callAjaxApiGetAllMenus();
  })
  $("#btn-add-data").on("click", onBtnAddDataClick) // gán sự kiện click vào nút thêm mới dữ liệu 
  gTable.on("click", ".btn-update-data", function() { // gán sự kiện click vào nút cập nhật dữ liệu
    onBtnUpdateDataClick(this)
  })
  gTable.on("click", ".btn-delete-data", function() { // gán sự kiện click vào nút xóa dữ liệu
    onBtnDeleteDataClick(this)
  })
  $("#btn-confirm-add-data").on("click", onBtnConfirmAddDataClick) // gán sự kiện click vào nút xác nhận thêm mới dữ liệu 
  $("#btn-confirm-update-data").on("click", onBtnConfirmUpdateDataClick) // gán sự kiện click vào nút xác nhận cập nhật dữ liệu
  $("#btn-confirm-delete-data").on("click", onBtnConfirmDeleteDataClick) // gán sự kiện click vào xác nhận xóa dữ liệu
  
  // VÙNG 3: hàm xử lý sự kiện
  function onBtnAddDataClick() { // xử lý sự kiện khi click vào nút thêm dữ liệu
    $("#modal-add-data").modal("show");
  }
  function onBtnUpdateDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút cập nhật dữ liệu
    "use strict";
    $("#modal-update-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    console.log("Nút Sửa được ấn. Id là: " + gSelectedId);
    loadDataToModalUpdateData(vRowData);
  }
  function onBtnDeleteDataClick(paramButtonElement) { // xử lý sự kiện khi click vào nút xóa dữ liệu
    "use strict";
    $("#modal-delete-data").modal("show");
    var vCurrentRow = $(paramButtonElement).closest("tr");
    var vRowData = gTable.row(vCurrentRow).data();
    gSelectedId = vRowData.id;
    $("#span-data-id").html(gSelectedId);
    console.log("Nút Xóa được ấn. Id là: " + gSelectedId);
  }
  function onBtnConfirmAddDataClick() { // xử lý sự kiện khi click vào nút xác nhận thêm dữ liệu
    gSelectedId = 0; // gán Id đã chọn là 0 nếu đang muốn add dữ liệu
    // B1: đọc dữ liệu từ form
    var vMenuData = readDataFromModalAddData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vMenuData);
    if (vIsValid == true) {
      // B3: gọi api
      callAjaxApiAddNewMenu(vMenuData);
      resetModalAddData();
      $("#modal-add-data").modal("hide");
    }
  }
  function onBtnConfirmUpdateDataClick() { // xử lý sự kiện khi click vào nút xác nhận cập nhật dữ liệu
    // B1: đọc dữ liệu từ modal update data
    var vMenuData = readDataFromModalUpdateData();
    // B2: validate dữ liệu
    var vIsValid = validateData(vMenuData);
    if(vIsValid == true) {
      // B3: gọi api
      callAjaxApiUpdateMenuById(gSelectedId, vMenuData);
      $("#modal-update-data").modal("hide")
    }
  }
  function onBtnConfirmDeleteDataClick() { // xử lý sự kiện khi click vào nút xác nhận xóa dữ liệu
    callAjaxApiDeleteMenuById(gSelectedId);
    $("#modal-delete-data").modal("hide")
  }

  // VÙNG 4: hàm dùng chung
  function callAjaxApiGetAllMenus() { // gọi api lấy list all dữ liệu
    $.ajax({
      url: gBaseUrl,
      type: "GET",
      async: false,
      dataType: "json",
      success: function(res) {
        console.log(res);
        gDatabase = res;
        loadListMenuToTable(gDatabase);
      },
      error: function(err) {
        console.log(err.response);
      }
    })
  }
  function loadListMenuToTable(paramListMenu) { // đổ list dữ liệu vào table
    "use strict";
    gTable.clear();
    gTable.rows.add(paramListMenu);
    gTable.draw();
  }
  function readDataFromModalAddData() { // đọc dữ liệu từ modal add data
    var vMenuData = {
      kichCo: $("#input-kichCo-modal-add-data").val().trim(),
      duongKinh: $("#input-duongKinh-modal-add-data").val().trim(),
      suon: $("#input-suon-modal-add-data").val().trim(),
      salad: $("#input-salad-modal-add-data").val().trim(),
      soLuongNuoc: $("#input-soLuongNuoc-modal-add-data").val().trim(),
      thanhTien: $("#input-thanhTien-modal-add-data").val().trim()
    };			
    console.log("Thông tin Menu đọc được là:");
    console.log(vMenuData);
    return vMenuData
  }
  function readDataFromModalUpdateData() { // đọc dữ liệu từ modal update data
    var vMenuData = {
      kichCo: $("#input-kichCo-modal-update-data").val().trim(),
      duongKinh: $("#input-duongKinh-modal-update-data").val().trim(),
      suon: $("#input-suon-modal-update-data").val().trim(),
      salad: $("#input-salad-modal-update-data").val().trim(),
      soLuongNuoc: $("#input-soLuongNuoc-modal-update-data").val().trim(),
      thanhTien: $("#input-thanhTien-modal-update-data").val().trim()
    };			
    console.log("Thông tin Menu đọc được là:");
    console.log(vMenuData);
    return vMenuData
  }
  function loadDataToModalUpdateData(paramMenu) { // đổ dữ liệu vào modal update data
    "use strict";
    $("#input-kichCo-modal-update-data").val(paramMenu.kichCo);
    $("#input-duongKinh-modal-update-data").val(paramMenu.duongKinh);
    $("#input-suon-modal-update-data").val(paramMenu.suon);
    $("#input-salad-modal-update-data").val(paramMenu.salad);
    $("#input-soLuongNuoc-modal-update-data").val(paramMenu.soLuongNuoc);
    $("#input-thanhTien-modal-update-data").val(paramMenu.thanhTien);
  }
  function validateData(paramMenuData) { // kiểm tra dữ liệu từ 2 modal
    "use strict";
    var vIsValid = false;
    if (paramMenuData.kichCo == "") {
      alert("Chưa nhập Kích cỡ")
    }
    else if (checkKichCoDaTonTai(paramMenuData.kichCo, gSelectedId)) {
      alert("Đã tồn tại Kích cỡ này");
    }
    else if (paramMenuData.duongKinh == "") {
      alert("Chưa nhập Đường kính")
    }
    else if (paramMenuData.duongKinh < 0 || Number.isInteger(Number(paramMenuData.duongKinh)) == false) {
      alert("Đường kính phải là số nguyên dương")
    }
    else if (paramMenuData.suon == "") {
      alert("Chưa nhập số Sườn nướng")
    }
    else if (paramMenuData.suon < 0 || Number.isInteger(Number(paramMenuData.suon)) == false) {
      alert("Số Sườn nướng phải là số nguyên dương")
    }
    else if (paramMenuData.salad == "") {
      alert("Chưa nhập số Salad")
    }
    else if (paramMenuData.salad < 0 || Number.isInteger(Number(paramMenuData.salad)) == false) {
      alert("Số Salad phải là số nguyên dương")
    }
    else if (paramMenuData.soLuongNuoc == "") {
      alert("Chưa nhập Số lượng Nước")
    }
    else if (paramMenuData.soLuongNuoc < 0 || Number.isInteger(Number(paramMenuData.soLuongNuoc)) == false) {
      alert("Số lượng Nước phải là số nguyên dương")
    }
    else if (paramMenuData.thanhTien == "") {
      alert("Chưa nhập Giá")
    }
    else if (paramMenuData.thanhTien < 0 || Number.isInteger(Number(paramMenuData.thanhTien)) == false) {
      alert("Giá phải là số nguyên dương")
    }
    else {
      vIsValid = true;
    }
    return vIsValid
  }
  function checkKichCoDaTonTai(paramKichCo, paramId) {
    "use strict";
    // nếu paramId = 0, hàm kiểm tra để add dữ liệu, nếu paramId khác 0, hàm kiểm tra dể cập nhật dữ liệu
    var vIsExisted = false;
    var bI = 0;
    while (vIsExisted == false && bI < gDatabase.length) {
      if (gDatabase[bI].kichCo == paramKichCo && gDatabase[bI].id != paramId) {
        // nếu trùng kích cỡ của item nhưng id cũng bị trùng chứng tỏ đang cập nhật dữ liệu của chính item đó, nên ko xem là bị trùng kích cỡ
        // phải trùng kích cỡ của item và khác id thì chứng tỏ cập nhật dữ liệu bị trùng kích cỡ với item khác,
        // hoặc đang add dữ liệu (vì khi add dữ liệu, paramId luôn là 0 nên luôn khác id của item)
        vIsExisted = true
      }
      else {
        bI ++
      }
    }
    return vIsExisted
  }
  function resetModalAddData() {
    $("#input-kichCo-modal-add-data").val("");
    $("#input-duongKinh-modal-add-data").val("");
    $("#input-suon-modal-add-data").val("");
    $("#input-salad-modal-add-data").val("");
    $("#input-soLuongNuoc-modal-add-data").val("");
    $("#input-thanhTien-modal-add-data").val("");
  }
  function callAjaxApiAddNewMenu(paramMenuData) { // gọi api thêm dữ liệu
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl,
      type: "POST",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramMenuData),
      success: function(res) {
        callAjaxApiGetAllMenus();
        console.log("Tạo Menu mới thành công. Response là:");
        console.log(res);
        alert("Tạo Menu mới thành công! Id mới là: " + res.id);
      },
      error: function(err){
        console.log(err.response);
        alert("Tạo Menu mới không thành công! Xem console");
      }
    })
  }  
  function callAjaxApiUpdateMenuById(paramId, paramMenuData) { // gọi api cập nhật dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramId,
      type: "PUT",
      contentType: "application/json; charset=UTF-8",
      data: JSON.stringify(paramMenuData),
      success: function(res) {
        callAjaxApiGetAllMenus();
        console.log("Update thành công cho Menu có id " + paramId + ". Response là:");
        console.log(res);
        alert("Update thành công cho Menu có id " + paramId);
      },
      error: function(err){
        console.log(err.response);
        alert("Update không thành công. Xem console");
      }
    })
  }
  function callAjaxApiDeleteMenuById(paramMenuId) { // gọi api xóa dữ liệu theo id
    "use strict";
    $.ajax({
      async: false,
      url: gBaseUrl + "/" + paramMenuId,
      type: "DELETE",
      dataType: "json",
      success: function(res) {
        callAjaxApiGetAllMenus();
        console.log("Xóa Menu có id " + paramMenuId + " thành công. Response là:");
        console.log(res);
        alert("Xóa Menu có id " + paramMenuId + " thành công!");
      },
      error: function(err){
        console.log(err.response);
        alert("Xóa Menu có id " + paramMenuId + " không thành công!");
      }
    })
  }
})
