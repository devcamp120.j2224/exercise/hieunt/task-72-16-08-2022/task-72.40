package com.apicrud.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apicrud.backend.model.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu, Integer> {

}
