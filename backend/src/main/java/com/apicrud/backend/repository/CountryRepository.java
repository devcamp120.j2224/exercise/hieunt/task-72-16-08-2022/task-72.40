package com.apicrud.backend.repository;

import java.util.List;

import com.apicrud.backend.model.CCountry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);//tìm country theo country code 
	List<CCountry> findByCountryCodeContaining(String countryCode);//tìm country theo country code chứa chuỗi (like %)
	List<CCountry> findByCountryName(String countryName);//tim country theo country name
	CCountry findByRegionsRegionCode(String regionCode);//tim country theo region code
	List<CCountry> findByRegionsRegionName(String regionName);
}
