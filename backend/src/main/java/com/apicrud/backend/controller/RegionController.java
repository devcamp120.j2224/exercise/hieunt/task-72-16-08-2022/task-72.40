package com.apicrud.backend.controller;

import com.apicrud.backend.model.*;
import com.apicrud.backend.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/regions")
@CrossOrigin(value = "*", maxAge = -1)
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private CountryRepository countryRepository;

	@GetMapping("")
	public List<CRegion> getAllRegion() {
		return regionRepository.findAll();
	}

	@GetMapping("/{id}")
	public Object getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/country/{countryId}")
	public ResponseEntity<Object> createRegion(@PathVariable("countryId") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CRegion newRole = new CRegion();
				newRole.setRegionName(cRegion.getRegionName());
				newRole.setRegionCode(cRegion.getRegionCode());				
				CCountry _country = countryData.get();
				newRole.setCountry(_country);
				CRegion savedRole = regionRepository.save(newRole);
				return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			return ResponseEntity.unprocessableEntity()
			.body("Failed to Create Region: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>("Country not found", HttpStatus.NOT_FOUND);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable Long id, @RequestBody CRegion cRegion) {
		Optional<CRegion> regionData = regionRepository.findById(id);
		if (regionData.isPresent()) {
			try {
				CRegion newRegion = regionData.get();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());
				CRegion savedRole = regionRepository.save(newRegion);
				return new ResponseEntity<>(savedRole, HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
				.body("Failed to Update Region: "+e.getCause().getCause().getMessage());
			}
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/* @GetMapping("/country/{countryId}")
    public List<CRegion> getRegionsByCountryId(@PathVariable Long countryId) {
        return regionRepository.findByCountryId(countryId);
    }
	
	@GetMapping("/country")
    public List<CRegion> getRegionsByCountryCode(@RequestParam String countryCode) {
        return regionRepository.findByCountryCountryCode(countryCode);
    }

	@GetMapping("/country/{countryId}/region/{regionId}")
    public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable Long countryId,@PathVariable Long regionId) {
        return regionRepository.findByIdAndCountryId(regionId, countryId);
    }

	@GetMapping("/country/{countryId}/count")
    public int countRegionByCountryId(@PathVariable Long countryId) {
        return regionRepository.findByCountryId(countryId).size();
    }
	
	@GetMapping("/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return regionRepository.existsById(id);
	}		 */
}

