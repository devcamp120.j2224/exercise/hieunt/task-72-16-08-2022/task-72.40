package com.apicrud.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apicrud.backend.model.CUser;


public interface IUserRepository extends JpaRepository<CUser, Long>{
    
}
