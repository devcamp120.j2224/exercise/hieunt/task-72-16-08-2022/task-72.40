package com.apicrud.backend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

import com.apicrud.backend.model.CDrink;
import com.apicrud.backend.repository.IDrinkRepository;

@Service
public class CDrinkService {
    @Autowired
	IDrinkRepository iDrinkRepository;

    public ArrayList<CDrink> getDrinkList() {
        ArrayList<CDrink> listDrink = new ArrayList<>();
        iDrinkRepository.findAll().forEach(listDrink::add);
        return listDrink;
    }
    
    public CDrink getDrinkById(long id) {
        Optional<CDrink> existedDrink = iDrinkRepository.findById(id);
        if (existedDrink.isPresent()) {
			return existedDrink.get();
		} else {
			return null;
		}
    }

    public CDrink createDrink(CDrink pDrink) {
        CDrink newDrink = new CDrink();
        newDrink.setMaNuocUong(pDrink.getMaNuocUong());
        newDrink.setTenNuocUong(pDrink.getTenNuocUong());
        newDrink.setDonGia(pDrink.getDonGia());
        newDrink.setGhiChu(pDrink.getGhiChu());
        newDrink.setNgayTao(new Date());
        newDrink.setNgayCapNhat(null);            
        return newDrink;
    }

    public CDrink updateDrinkById(long id, CDrink pDrink) {
        Optional<CDrink> existedDrink = iDrinkRepository.findById(id);
		if (existedDrink.isPresent()) {
			CDrink updatedDrink = existedDrink.get();
			updatedDrink.setMaNuocUong(pDrink.getMaNuocUong());
            updatedDrink.setTenNuocUong(pDrink.getTenNuocUong());
			updatedDrink.setDonGia(pDrink.getDonGia());
            updatedDrink.setGhiChu(pDrink.getGhiChu());
			updatedDrink.setNgayCapNhat(new Date());
            return updatedDrink;
		} else {
            return null;
        }
    }

}
